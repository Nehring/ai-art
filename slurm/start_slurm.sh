#!/bin/bash

'''
how to execute:

./usrun.sh -p RTX6000 --gpus=1 /netscratch/nehring/projects/rasa-nlu-experiment/start_slurm.sh rasa_eval.py

'''

export TORCH_HOME=/netscratch/nehring/cache/torch
export PIP_CACHE_DIR=/netscratch/nehring/cache/pip

cd /netscratch/nehring/projects/img/ai-art
python process.py