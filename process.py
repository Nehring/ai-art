from imagedb import iterate_all, imagedb_path
from transfer import load_images, run_style_transfer, device, imshow
import torchvision.models as models
import torch
import traceback
import os
import shutil
import random

if __name__ == "__main__":

    print("loading model")
    cnn = models.vgg19(pretrained=True).features.to(device).eval()
    cnn_normalization_mean = torch.tensor([0.485, 0.456, 0.406]).to(device)
    cnn_normalization_std = torch.tensor([0.229, 0.224, 0.225]).to(device)

    output_folder="output"
    if os.path.exists(output_folder):
        shutil.rmtree(output_folder)
    os.mkdir(output_folder)

    pairs=list(iterate_all())
    random.shuffle(pairs)
    c=0
    for pair in pairs:
        try:
            print(pair.describe())
            style_img, input_img, content_img=load_images(pair.style_path, pair.content_path)

            outimages = run_style_transfer(cnn, cnn_normalization_mean, cnn_normalization_std,
                                        content_img, style_img, input_img, num_steps=300)

            for i in range(len(outimages)):
                outfile=os.path.join(output_folder, pair.get_filename(praefix=str(c) + "_", suffix="_" + str(i)))
                imshow(outimages[i], outfile)
            c+=1

        except Exception:
            print(traceback.format_exc())

