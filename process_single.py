import argparse
import os

from proc.imagedb import ImagePair
from proc.transfer import load_images, run_style_transfer, device, imshow, get_model, get_normalizations

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('-content', type=str, required=True)
    parser.add_argument('-style', type=str, required=True)
    parser.add_argument('-out', type=str, default="output")

    args = parser.parse_args()

    if not os.path.exists(args.content):
        raise Exception("content file does not exist")

    if not os.path.exists(args.style):
        raise Exception("content style does not exist")

    pair=ImagePair(args.content, args.style)

    style_img, input_img, content_img=load_images(pair.style_path, pair.content_path)

    cnn=get_model()
    cnn_normalization_mean, cnn_normalization_std=get_normalizations()
    
    outimages = run_style_transfer(cnn, cnn_normalization_mean, cnn_normalization_std,
                                content_img, style_img, input_img, num_steps=300)

    if not os.path.exists(args.out):
        os.mkdir(args.out)

    for i in range(len(outimages)):
        outfile=os.path.join(args.out, pair.get_filename(suffix="_" + str(i)))
        imshow(outimages[i], outfile)
        print("wrote output file", outfile)
