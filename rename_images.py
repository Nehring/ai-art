import argparse
import os


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Rename all images in a folder to 0.jpg, 1.jpg, ...')
    parser.add_argument('-folder', type=str, required=True)

    args = parser.parse_args()

    i=0
    for f in os.listdir(args.folder):
        ext=f[f.rfind("."):]
        new_name=os.path.join(args.folder, str(i) + ext)
        old_name=os.path.join(args.folder, f)
        os.rename(old_name, new_name)
        i+=1

        print(old_name, "->", new_name)
