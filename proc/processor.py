from imagedb import iterate_pairs, imagedb_path
from transfer import load_images, run_style_transfer, device, imshow, get_model, get_normalizations
import torchvision.models as models
import torch
import traceback
import os
import shutil
import random

def process(content_folder, style_folder, model=None):

    print("loading model")
    if model is None:
        model=get_model()
    cnn_normalization_mean, cnn_normalization_std = get_normalizations

    output_folder="output"
    if os.path.exists(output_folder):
        shutil.rmtree(output_folder)
    os.mkdir(output_folder)

    pairs=list(iterate_pairs(content_folder, style_folder))
    #random.shuffle(pairs)
    c=0
    for pair in pairs:
        try:
            print(pair.describe())
            style_img, input_img, content_img=load_images(pair.style_path, pair.content_path)

            outimages = run_style_transfer(model, cnn_normalization_mean, cnn_normalization_std,
                                        content_img, style_img, input_img, num_steps=300)

            for i in range(len(outimages)):
                outfile=os.path.join(output_folder, pair.get_filename(praefix=str(c) + "_", suffix="_" + str(i)))
                imshow(outimages[i], outfile)
            c+=1

        except Exception:
            print(traceback.format_exc())

