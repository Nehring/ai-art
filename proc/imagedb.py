from PIL import Image
import os

imagedb_path="../images/"

class ImagePair():

    def __init__(self, content_path, style_path):
        self.content_path=content_path
        self.style_path=style_path

    def get_content_filename(self):
        name=os.path.basename(self.content_path)
        name=name[0:name.rfind(".")]
        return name

    def get_style_filename(self):
        name=os.path.basename(self.style_path)
        name=name[0:name.rfind(".")]
        return name

    def describe(self):
        desc ="ImagePair("
        desc +=self.content_path[len(imagedb_path):] + ", "
        desc += self.style_path[len(imagedb_path):] + ")"
        return desc

    def get_filename(self, praefix="", suffix=""):

        def get_dirname(s):
           s=os.path.dirname(s)
           s=s[s.rfind("/")+1:] + "_"
           return s

        fn = praefix
        fn +=get_dirname(self.content_path)
        fn += self.get_content_filename()
        fn += "_"
        fn += get_dirname(self.style_path)
        fn += self.get_style_filename()
        fn += suffix
        fn += ".jpeg"
        return fn

def iterate_image_folder(folder):
    for f in os.listdir(folder):
        if f==".":
            continue
        path=os.path.join(folder, f)
        yield path

def iterate_pairs(content_folder, style_folder):

    first=True
    for content_img in iterate_image_folder(content_folder):
        for style_img in iterate_image_folder(style_folder):
            yield ImagePair(content_img, style_img)


def iterate_all():
    stylefolder=os.path.join(imagedb_path, "styles")
    content_folder=os.path.join(imagedb_path, "content")
    for style in os.listdir(stylefolder):
        for content in os.listdir(content_folder):
            lstyle=os.path.join(stylefolder, style)
            lcontent=os.path.join(content_folder, content)
            for pair in iterate_pairs(lcontent, lstyle):
                yield pair
